CREATE TABLE `data`.`department` (
  `department_no` INT NOT NULL,
  `department_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`department_no`));
