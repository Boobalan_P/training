
SELECT employee1.first_name AS Name1, employee2.first_name AS Name2, employee1.area
FROM employee employee1, employee employee2
WHERE employee1.department_no = employee2.department_no
AND employee1.area = employee2.area