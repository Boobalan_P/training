

ALTER TABLE `data`.`employee` 
ADD CONSTRAINT `department_no`
  FOREIGN KEY (`department_no`)
  REFERENCES `data`.`department` (`department_no`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
