SELECT department.department_name
      ,max(employee.annual_salary) AS highpaid
      ,min(employee.annual_salary) AS leastpaid
 FROM  employee employee
	  ,department department
 WHERE employee.department_no=department.department_no
 GROUP BY department.department_no ;