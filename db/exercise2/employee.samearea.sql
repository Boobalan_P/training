SELECT employee.first_name
      ,employee.area AS same_area
	  ,department.department_name AS same_department
  FROM employee employee,department department 
  WHERE employee.department_no = department.department_no
  AND employee.area IN('chennai')
  AND department.department_name IN('Engineering');
  
      