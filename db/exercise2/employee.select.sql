SELECT employee.first_name
      ,employee.surname
      ,department.department_name
FROM data.employee employee
     ,data.department department
WHERE employee.department_no = department.department_no;