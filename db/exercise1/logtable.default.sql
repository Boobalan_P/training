CREATE TABLE school.log_table (
             student_id INT NOT NULL
             ,student_name VARCHAR(45) NOT NULL DEFAULT 'Ravi'
             ,teacher_name VARCHAR(45) NOT NULL
             ,group_name VARCHAR(45) NOT NULL
             ,PRIMARY KEY (student_id)
             ,INDEX student_id USING BTREE (student_id) VISIBLE);