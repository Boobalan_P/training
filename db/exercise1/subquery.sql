SELECT student_id
       ,student_name
       ,student_class
       ,student_age
FROM school.student_table
WHERE student_class>
      (SELECT teacher_id
		FROM school.teacher_table
        WHERE teacher_id=3);