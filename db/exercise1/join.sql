SELECT student_table.student_id
	   ,student_table.student_name
       ,student_table.student_class
       ,teacher_table.teacher_name
FROM school.student_table
CROSS JOIN school.teacher_table;
SELECT  mark_table.student_id
       ,mark_table.subject
       ,mark_table.mark
       ,teacher_table.teacher_name
FROM school.mark_table
LEFT JOIN school.teacher_table
ON mark_table.student_id = teacher_table.teacher_id;
SELECT student_table.student_id
       ,student_table.student_name
       ,student_table.student_class
       ,teacher_table.teacher_name
FROM school.student_table
RIGHT JOIN school.teacher_table
ON student_table.student_id = teacher_table.teacher_id;
SELECT student_table.student_id
       ,student_table.student_name
       ,student_table.student_class
       ,teacher_table.teacher_name
FROM school.student_table
LEFT JOIN school.teacher_table
ON student_table.student_id = teacher_table.teacher_id;
SELECT student_table.student_id
       ,student_table.student_name
       ,student_table.student_class
       ,teacher_table.teacher_name
FROM school.student_table
LEFT JOIN school.teacher_table
ON student_table.student_id = teacher_table.teacher_id
UNION ALL
SELECT student_table.student_id
	   ,student_table.student_name
       ,student_table.student_class
       ,teacher_table.teacher_name
FROM school.student_table
RIGHT JOIN school.teacher_table
ON student_table.student_id = teacher_table.teacher_id;
SELECT student_table.student_id
       ,student_table.student_name
       ,student_table.student_class
       ,teacher_table.subject
  FROM    school.student_table
RIGHT JOIN school.teacher_table
ON student_table.student_id = teacher_table.teacher_id; 
SELECT student_table.student_id
       ,student_table.student_name
       ,student_table.student_class
       ,mark_table.mark
	   ,mark_table.subject
FROM school.student_table
INNER JOIN school.mark_table
ON student_table.student_id = mark_table.student_id;
