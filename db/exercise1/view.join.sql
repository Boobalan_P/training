CREATE VIEW crossjoin AS
SELECT student_table.student_id
       ,student_table.student_name
       ,student_table.student_class
       ,teacher_table.teacher_name
FROM school.student_table
CROSS JOIN school.teacher_table;

SHOW TABLES;

SELECT*FROM crossjoin;