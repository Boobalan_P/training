SELECT  mark_table.student_id
       ,mark_table.subject
       ,mark_table.mark
       ,teacher_table.teacher_name
FROM school.mark_table
LEFT JOIN school.teacher_table
ON mark_table.student_id = teacher_table.teacher_id;