SELECT student_table.student_id
       ,student_table.student_name
       ,student_table.student_class
       ,teacher_table.teacher_name
FROM school.student_table
RIGHT JOIN school.teacher_table
ON student_table.student_id = teacher_table.teacher_id;