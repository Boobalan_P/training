SELECT student.roll_number
       ,student.name
       ,student.dob
       ,student.gender
       ,student.email
       ,student.phone
       ,student.address
       ,student.academic_year
       ,semester_result.semester
       ,semester_result.gpa
  FROM student student
 INNER JOIN semester_result ON semester_result.stud_id = student.id
   AND (gpa > '8' AND semester = '2');
SELECT student.roll_number
       ,student.name
       ,student.dob
       ,student.gender
       ,student.email
       ,student.phone
       ,student.address
       ,student.academic_year
       ,semester_result.semester
       ,semester_result.gpa
  FROM student student
 INNER JOIN semester_result ON semester_result.stud_id = student.id
   AND (gpa > '5' AND semester = '2');