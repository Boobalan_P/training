SELECT student.id AS roll_number
       ,student.name AS student_name
       ,student.gender
       ,student.dob
       ,student.address AS student_address
       ,student.email
       ,student.phone
       ,college.name AS college_name
       ,college.city AS college_city
       ,department.dept_name
       ,university.university_name
  FROM university.student
 INNER JOIN university.college_department
    ON student.cdept_id = college_department.cdept_id
 INNER JOIN university.department
	ON department.dept_code =  college_department.udept_code
 INNER JOIN university.college
    ON college.id =  college_department.college_id
 INNER JOIN university.university
    ON college.univ_code = university.univ_code
 WHERE university.university_name IN ('anna')
   AND college.city IN ('chennai')
   AND student.academic_year = '2007';