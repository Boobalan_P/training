SELECT university.university_name
       ,student.id AS roll_number
       ,student.name AS student_name
       ,student.gender
       ,student.dob
       ,student.address
       ,college.name AS college_name
       ,department.dept_name
       ,semester_result.semester
       ,semester_result.grade
       ,semester_result.credits
  FROM university.student student
 INNER JOIN university.college_department
    ON student.cdept_id = college_department.cdept_id
 INNER JOIN university.college
    ON college.id = college_department.college_id
 INNER JOIN university.university
    ON university.univ_code = college.univ_code
 INNER JOIN university.syllabus
    ON syllabus.cdept_id = college_department.cdept_id
 INNER JOIN university.department
    ON department.dept_code = college_department.udept_code
 INNER JOIN university.semester_result
    ON student.id = semester_result.stud_id
 ORDER BY college.name 
       ,semester_result.semester ;