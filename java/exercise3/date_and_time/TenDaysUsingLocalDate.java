/*
 * Requirement:
 *  Write a Java program to get the dates 10 days before and after today.
        1)using calendar
        2)using local date 
        
 * Entity:
 *    TenDaysUsingLocalDate
 *  
 * Method Signature:
 *    public static void main(String[] args) 
 *  
 * Jobs To Be Done:
 *    1)Create a reference for LocalDate class.
 *    2)Print the date, After 10 days from current date.
 *    3)Print the date, Before 10 days from current date.
 *    
 * Pseudo code:
 *  class TenDaysUsingLocalDate {

		public static void main(String[] args) {
			LocalDate today = LocalDate.now();
			System.out.println("After 10 days " + today.plusDays(10));
			System.out.println("Before 10 days " + today.minusDays(10));

		}

	}
   
 *    
 */
package com.kpr.training.date_and_time;

import java.time.LocalDate;

public class TenDaysUsingLocalDate {

	public static void main(String[] args) {
		LocalDate today = LocalDate.now();
		System.out.println("After 10 days " + today.plusDays(10));
		System.out.println("Before 10 days " + today.minusDays(10));

	}

}
