/* 
 * Requirement:
 *    Write a Java program to get a date before and after 1 year compares to the current date. 
 *    
 * Entity:
 *    CurrentDate
 *    
 * Method Signature:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1)Create a reference for Calendar class.
 *    2)Add one year, before and after from current date in calendar.
 *    3)Print the current date.
 *    4)Print the date, before and after one year from current date.
 *    
 * Pseudo Code:
 * class CurrentDate {

	public static void main(String[] args) {
	Calendar calendar = Calendar.getInstance();
    Date currentDate = calendar.getTime();
    calendar.add(Calendar.YEAR, 1); 
    Date nextYear = calendar.getTime();
    calendar.add(Calendar.YEAR, -2); 
    Date previousYear = calendar.getTime();
    System.out.println("Current Date : " + currentDate);
    System.out.println("Date before 1 year : " + previousYear);
    System.out.println("Date after 1 year  : " + nextYear);  	
  }
}

*/
package com.kpr.training.date_and_time;

import java.util.Calendar;
import java.util.Date;

public class CurrentDate {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		Date currentDate = calendar.getTime();
		
		calendar.add(Calendar.YEAR, 1);
		Date nextYear = calendar.getTime();
		
		calendar.add(Calendar.YEAR, -2);
		Date previousYear = calendar.getTime();
		
		System.out.println("Current Date : " + currentDate);
		System.out.println("Date before 1 year : " + previousYear);
		System.out.println("Date after 1 year  : " + nextYear);
	}
}


