/*
 * Requirement:
 *   Write a Java program to calculate your age 
 * 
 * Entity:
 *   AgeCalculation
 * 
 * Method Signature:
 *   public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 	 1)Create a reference for LocalDate class and assigned to the date of birth.
 *   2)Create a reference for Period class and find out the period between date of birth and now in local date.
 *   3)Print the age.
 *   
 * Pseudo Code:
 * class AgeCalculation {

		public static void main(String[] args) {
			LocalDate dateOfBirth = LocalDate.of(2000, 11, 22);
			Period period = Period.between(dateOfBirth, LocalDate.now());
			System.out.print(period.getYears() + " years ");
		}
	}
 */

package com.kpr.training.date_and_time;

import java.time.LocalDate;
import java.time.Period;

public class AgeCalculation {

	public static void main(String[] args) {
		LocalDate dateOfBirth = LocalDate.of(2000, 11, 22);
		Period period = Period.between(dateOfBirth, LocalDate.now());
		System.out.print(period.getYears() + " years " );
	}
}


