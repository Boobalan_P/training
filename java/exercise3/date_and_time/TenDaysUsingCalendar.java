/*
 * Requirement:
 *    Write a Java program to get the dates 10 days before and after today.
 *    1)using calendar
 *    2)using local date 
 *    
 * Entity:
 * 	  TenDaysUsingCalendar
 * 
 * Method Signature:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1)Create a reference for Calendar class.
 *    2)Add 10 days, before and after from current date in calendar.
 *    3)Print the currentDate.
 *    4)Print the date, before and after 10 days from current date.
 *    
 * Pseudo Code:
 * class TenDaysUsingCalendar {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
	    Date currentDate = calendar.getTime();
	    calendar.add(Calendar.DAY_OF_MONTH, 10); 
	    Date afterTenDays = calendar.getTime();
	    calendar.add(Calendar.DAY_OF_MONTH, -20); 
	    Date beforeTenDays = calendar.getTime();
	    System.out.println("Current Date : " + currentDate);
	    System.out.println("Date before 10 days : " + beforeTenDays);
	    System.out.println("Date after 10 days  : " + afterTenDays);  	
	  }
	}



 */


package com.kpr.training.date_and_time;

import java.util.Calendar;
import java.util.Date;

public class TenDaysUsingCalendar {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		Date currentDate = calendar.getTime();

		calendar.add(Calendar.DAY_OF_MONTH, 10);
		Date afterTenDays = calendar.getTime();

		calendar.add(Calendar.DAY_OF_MONTH, -20);
		Date beforeTenDays = calendar.getTime();

		System.out.println("Current Date : " + currentDate);
		System.out.println("Date before 10 days : " + beforeTenDays);
		System.out.println("Date after 10 days  : " + afterTenDays);
	}
}


