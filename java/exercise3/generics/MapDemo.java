/*
 * Requirements : 
 *   To write a program to demonstrate generics - for loop for map.
 *
 * Entities :
 *     MapDemo 
 *    
 * Function Signature :
 *     public static <K, E> void printMap(HashMap<K, E> map)
 *     public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *     1)Creating the class Map.
 *     2)Defining the method printMap and print the elements in the map
 *     3)Adding elements to the map with put method 
 *     4)Iterating the map with for loop.
 */
package com.kpr.training.generics;

import java.util.HashMap;
import java.util.Scanner;

public class MapDemo {

	public static <K, E> void printMap(HashMap<K, E> map) {
		for (K key : map.keySet()) {
			E value = map.get(key);
			System.out.println(key + " : " + value);
		}
	}

	public static void main(String[] args) {
		HashMap<Integer, String> firstMap = new HashMap<Integer, String>();
		Scanner scanner = new Scanner(System.in);
		int range = scanner.nextInt();
		for (int element = 0; element < range; element++) {
			firstMap.put(scanner.nextInt(), scanner.nextLine());
		}
		printMap(firstMap);

		HashMap<String, Integer> secondMap = new HashMap<>();
		for (int element = 0; element < range; element++) {
			secondMap.put(scanner.nextLine(), scanner.nextInt());
		}
		printMap(secondMap);
	}
}
