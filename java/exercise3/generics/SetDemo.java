/*
 * Requirements : 
 *   To write a program to demonstrate generics - for loop for set.
 *
 * Entities :
 *    public class SetDemo 
 *    
 * Function Signature :
 *    	public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *     1)Create a reference for HashSet with String type.
 *     2)Add some names to the Set.
 *     3)Create a reference for iterator .
 *     4)for each element in the set.
 *     	   4.1)Print the element.
 *     5)for each element in the set.
 *     	   5.1)Print the element.
 * PseudoCode:
 * 	public class SetDemo {

		public static void main(String[] args) {
			HashSet<String> names = new HashSet<String>();
			//Add some names to the set.
			System.out.println("Iterating String set using while loop");
			
			//create reference for iterator.
			Iterator<String> iterator = names.iterator();
			while (iterator.hasNext()) {
				System.out.println(iterator.next());
			}
	
			System.out.println("Iterating String set using for loop");
			for (String name : names) {
				System.out.println(name);
			}
		}
    }
 */
package com.kpr.training.generics;

import java.util.HashSet;
import java.util.Iterator;

public class SetDemo {

	public static void main(String[] args) {
		HashSet<String> names = new HashSet<String>();
		String firstName = "Boobalan";
		names.add(firstName);
		names.add("Arya");
		names.add("Surya");
		names.add("Ajith");
		names.add("Ajith");
		System.out.println("Iterating String set using while loop");
		Iterator<String> iterator = names.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}

		System.out.println("Iterating String set using for loop");
		for (String name : names) {
			System.out.println(name);
		}
	}
}