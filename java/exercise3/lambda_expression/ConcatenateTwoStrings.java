/*
 * Requirements : 
 * 		 write a Lambda expression program with a single method interface to concatenate two strings
 * Entities :
 * 		MyInterface,
 * 		ConcatenateTwoStrings.
 * Method Signature :
 * 		String concat(String one, String two),
 * 		public static void main(String[] args).
 * Jobs To Be Done:
 * 		1)Creating the string variables and assign values.
 * 		2)Combine the two stings using lambda expression. The strings are combined using + operator
 * 	
 * 
 * Pseudo code:
 * 	
 * interface MyInterface {
 * 
 *     String concat(String one, String two);
 * }
 * class ConcatenateTwoStrings {
 * 
 *     public  static void main(String[] args) {
 *         //declare two string fields
 *         String one = "Boobalan";
 *         String two = " P";
 *         
 *         //lambda expression to add two strings
 *         MyInterface test = (string, string1) -> string + string1;
 *         
 *         //printing the resultant string
 *         System.out.println(test.concat(one, two));
 *     }
 * }
 */
package com.kpr.training.lambda_expression;

interface MyInterface {

	String concat(String one, String two);
}


public class ConcatenateTwoStrings {

	public static void main(String[] args) {

		String one = "Boobalan";
		String two = " P";
		MyInterface test = (string, string1) -> string + string1;
		System.out.println(test.concat(one, two));
	}
}
