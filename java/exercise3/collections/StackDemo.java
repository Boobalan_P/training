/*
Requirement:
    Add and remove the elements in stack.      
Entity:
    StackDemo

Method Signature:
    public static void main(String[] args)

Jobs To Be Done:
    1) Create reference to stack.
    2) Add the values to Stack.
    3) Print the stack .
    4) Remove some of the stack elements.
    5) Print the stack .
   
 Pseudo code:
 class StackDemo {

    public static void main(String[] args) {
        Stack<Integer> sample = new Stack<>();
        //Add the values in the stack
         * System.out.println(sample);
        //remove element from the stack
        System.out.println("After removing the some of element :" + sample);
        }
    }
 
*/
package com.kpr.training.collections;

import java.util.Stack;

public class StackDemo {

    public static void main(String[] args) {
        Stack<Integer> sample = new Stack<>();
        sample.push(12);
        sample.push(14);
        sample.push(16);
        sample.push(18);
        sample.push(77);
        System.out.println(sample);
        sample.remove(2);
        sample.remove(1);
        System.out.println("After removing the some of element :" + sample);
    }
}