/*
 * Requirement:
 *      java program to demonstrate adding elements, displaying, removing, and iterating in hash set
 
 * Entity:
 *      HashSetExample
 
 * Method Signature:
 *      public static void main(String[] args)
 
 * Jobs to be done:
 *    1.Create a reference for HashSet with type Integer.
 *    2.Add  the elements to the Set.
 *    3.Print the elements in the HashSet.
 *    4.Remove the element from the HashSet.
 *    5.Print the elements in the HashSet.
 *    6.for each element in the set
 *          6.1.Print the element
 *    
 * Pseudo Code:
 * class HashsetExample {

   		public static void main(String[] args) {
			HashSet<Integer> set = new HashSet<>();
			//Add the elements to the HashSet
			System.out.println("Displaying element in set " + set);
		
			set.remove(element);
			System.out.println("Printing the set after removing the element " + set);
		
			//Using iterator
			System.out.println("Displaying element using iterating method ");
			for (int element : set) {
				System.out.print(element + " ");
			}
		}
	}
		 
 * 
 */

package com.kpr.training.collections;

import java.util.HashSet;

public class HashsetExample {

	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<>();
		set.add(10);
		set.add(20);
		set.add(30);
		set.add(40);
		System.out.println("Displaying element in set " + set);
		set.remove(10);
		System.out.println("Printing the set after removing the element " + set);
		System.out.println("Displaying element using iterating method ");
		for (int element : set) {
			System.out.print(element + " ");
		}
	}
}