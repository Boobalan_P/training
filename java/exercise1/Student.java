package com.college.training.core;

//import com.college.training.Student.Name; 


public class Student {
    private String studentName;
    
    public int studentId;
    
    public Student() {
       // non-parameterized constructor 
    
    }
    protected Student(int Id , String name) {
        studentId = Id ;                              // parameterized constructor
        studentName = name;
    }
    {
        studentName = "Arun";                        // object initialization
        studentId = 11;
    }
    public void printInfo() {
        System.out.println(" StudentName: " + studentName + " StudentId: " + studentId);
    }
    
    public static void main(String[] args) {
        Student stud = new Student();
        //System.out.println( "StudentName: " + stud.studentName + " StudentId: " + stud.studentId ); // prints Arun 11
        Student stud1 = new Student(12,"Gokul");
        //System.out.println( "StudentName: " + stud1.studentName + " StudentId: " + stud1.studentId ); // prints Gokul 12
        stud.printInfo();
        stud1.printInfo();
    }
}    
