package com.college.training.department;
import com.college.training.core.Student;
public class  Department{
    
    private String departmentName;
    public int departmentId;
    
    protected Department(String name, int Id) {
        departmentName = name;
        departmentId = Id;
    }
    public void printInfo() {
        System.out.println("DepartmentName: " + departmentName + " DepartmentId: " + departmentId );
    }
    public static void main(String[] args) {
    Department department = new Department("EEE",1);
    com.college.training.core.Student stud1 = new com.college.training.core.Student();
    department.printInfo();
    stud1.printInfo();
    }
}