package com.bank.training.core;


public abstract class Bank{
    public String bankName;
    public int branchId;
    
    public Bank(String name,int id) {
        this.bankName = name;
        this.branchId = id;
    }
    
    public abstract void Account();
    
    
    
}
