import com.bank.training.core.Bank;

public class Customer extends Bank {
    protected long customerId;
    
    public Customer(String name, int id, long customerId) {
        super(name, id);
        this.customerId = customerId;
    }
    public void Account() {
        if(customerId == 0) {
            System.out.println("No such Customer is in our bank");
        }
        else {
            System.out.println(this.customerId + " " + "is in our bank");
        }
    }
    
    public static void main(String[] args) {
        Bank bank = new Customer("SBI", 1,234566);
        Bank customer = new Customer("KVB", 34,121223);
        bank.Account();
        customer.Account();
    }
}