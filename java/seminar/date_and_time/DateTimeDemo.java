package date_and_time;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeDemo {

	public static void main(String[] args) {
		Date date = new Date();
		Locale locale = new Locale("en", "UK");
		System.out.println("Today Date is :" + date);

		String date1 = DateFormat
				.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, locale).format(date);
		System.out.println("DEFAULT Date is:" + date1); // Sep 6, 2020 HH:mm:ss

		String date2 = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, locale)
				.format(date);
		System.out.println("SHORTED Date is:" + date2); // 9/6/20 HH:mm

		String date3 = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, locale)
				.format(date);
		System.out.println("MEDIUM Date is:" + date3); // Sep 6, 2020 HH:mm:ss

		String date4 = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale)
				.format(date);
		System.out.println("LONG Date is:" + date4); // September 6, 2020 HH:mm:ss + TimeZone

		String date5 = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, locale)
				.format(date);
		System.out.println("FULL Date is:" + date5); // Sunday, September 6, 2020 HH:mm:ss +
														// TimeZone
	}
}
