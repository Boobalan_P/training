package date_and_time;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeZoneDemo {

	public static void main(String[] args) {
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/London"));
		System.out.println(simpleDateFormat.format(date));
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
		System.out.println(simpleDateFormat.format(date));
	}
}
