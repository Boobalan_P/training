package date_and_time;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatDemo {

	public static void main(String[] args) {
		Date date = new Date();
		Locale locale = new Locale("en", "UK"); // represents (Language, country standards etc..)
		System.out.println("Today Date is :" + date);

		String date1 = DateFormat.getDateInstance(DateFormat.DEFAULT, locale).format(date);
		System.out.println("DEFAULT Date is:" + date1); // sep 6,2020

		String date2 = DateFormat.getDateInstance(DateFormat.SHORT, locale).format(date);
		System.out.println("SHORTED Date is:" + date2); // 9/6/20

		String date3 = DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(date);
		System.out.println("MEDIUM Date is:" + date3); // sep 6,2020

		String date4 = DateFormat.getDateInstance(DateFormat.LONG, locale).format(date);
		System.out.println("LONG Date is:" + date4); // september 6,2020

		String date5 = DateFormat.getDateInstance(DateFormat.FULL, locale).format(date);
		System.out.println("FULL Date is:" + date5); // sunday,september 6,2020
	}
}
