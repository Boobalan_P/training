package date_and_time;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SimpleDateFormatDemo {

	public static void main(String[] args) {
		Date date = new Date();
		System.out.println(date); // Sun Sep 06 HH:mm:ss + TimeZone 2020
		Locale locale = new Locale("en", "IN");
		String string = "EEE dd-MM-yyy HH:mm:ss.SSSZ";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(string, locale);
		String date1 = simpleDateFormat.format(date);
		System.out.println(date1); // Sun 06-09-2020 HH:mm:ss
	}

}
