package date_and_time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleDateUsingParseDemo {

	public static void main(String[] args) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = simpleDateFormat.parse("2020/13/31");
		System.out.println("Date is: " + date);
	}
}
