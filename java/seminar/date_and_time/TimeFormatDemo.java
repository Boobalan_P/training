package date_and_time;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeFormatDemo {

	public static void main(String[] args) {
		Date date = new Date();
		System.out.println("Time for Now:" + date);
		Locale locale = new Locale("en", "IN");

		String time1 = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(date);
		System.out.println("DEFAULT Time is:" + time1); // HH:mm:ss (AM or PM)

		String time2 = DateFormat.getTimeInstance(DateFormat.SHORT, locale).format(date);
		System.out.println("SHORTED Time is:" + time2); // HH:mm (AM or PM)

		String time3 = DateFormat.getTimeInstance(DateFormat.MEDIUM, locale).format(date);
		System.out.println("MEDIUM Time is:" + time3); // HH:mm:ss (AM or PM)

		String time4 = DateFormat.getTimeInstance(DateFormat.LONG, locale).format(date);
		System.out.println("LONG Time is:" + time4); // HH:mm:ss (AM OR PM) + TimeZone

		String time5 = DateFormat.getTimeInstance(DateFormat.FULL, locale).format(date);
		System.out.println("FULL Time is:" + time5); // HH:mm:ss (AM or PM) + TimeZone
	}
}
