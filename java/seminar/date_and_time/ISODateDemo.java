package date_and_time;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ISODateDemo {

	public static void main(String[] args) {
		Date date = new Date();
		String string = "yyyy-MM-dd HH:mm:ssZ";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(string);
		String date1 = simpleDateFormat.format(date);
		System.out.println(date1); // 2020-09-06 HH:mm:ssZ
	}
}
