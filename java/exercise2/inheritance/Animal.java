/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects.

Entities:
    Animal

Function Signature:
    public void sound()
    public void run()
    public void run(int kilometer)

Jobs to be Done:
    1) Declare the method sound.
        1.1)print the statement.
    2) Declare the method run.
        2.1)print the statement.
    3) Declare the method run with parameter 
        3.1)print the statement.
    
    */

package com.kpr.training.inheritance;

public class Animal {
	public void sound() {
		System.out.println(" Animal class ");

	}

	public void run() {
		System.out.println(" The animal is run ");
	}

	public void run(int kilometers) {
		System.out.println(" The animal is run few kilometers ");

	}
}


