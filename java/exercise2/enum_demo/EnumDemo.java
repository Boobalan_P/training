/*
Requirement:
    To compare the enum values using equals method and == operator.

Entity:
    EnumDemo.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
	1)Create a variable and assign null.
    2)Create a enum type Animal and store the values.
    3)Create a enum type Color and store the values.
    4)Compare using "==" operator.
        4.1)Check if the two Strings point to the same address,then print true.
        4.2)otherwise print false.
    5)Compare using equals.
        5.1)check if the two strings are equal,then print true.
        5.2)otherwise print false.
    6)Add a new element to the enum Color.
    7)Compare using "==" operator.
        7.1)Check if the two Strings point to the same address,then print true.
        7.2)otherwise print false.
    8)Compare using equals.
        8.1)check if the two strings are equal,then print true.
        8.2)otherwise print false.
    
    
*/

//Answer:
package com.kpr.training.enum_demo;

public class EnumDemo {

    public enum Animal {
        Lion,
        Tiger,
        Cat,
        Dog,
        Cow,
        Elephant,
        Deer
    }

    public enum Color {
        Yellow,
        Red,
        Sandle,
        Brown,
        Black,
        Gray,
        Orange
    }

    public static void main(String[] args) {
        Animal animal = null;
        System.out.println(animal == Animal.Dog);                // prints false
        System.out.println(Color.Gray.equals(Animal.Elephant)); // prints false
        Color color = Color.Brown;
        System.out.println(color == Color.Brown);             // prints true
        System.out.println(color.equals(Color.Brown));        // prints true
    }
}