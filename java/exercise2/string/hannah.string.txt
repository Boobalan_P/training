/*
Requirement:
    To find length of the given String and find the character value at 12 th position and also find the letter b in which position.
    String hannah = "Did Hannah see bees? Hannah did.";

Entities:
    There is no entity.
       
Function Signature:
    No, function declared.
    
Jobs to be done:
    1)Find the length of the given string.
    2)Find the character value at 12th position from the given String.
    3)Find the letter "b" in which position from the given String.

Solution:
    1)32
    2)e
    3)hannah.charAt(15)